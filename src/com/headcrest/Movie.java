package com.headcrest;

public class Movie extends MediaItem {

    private MediaMetadata metadata;

    public Movie(String title) {
        super(title);
    }

    public Movie(String title, String director, String year){
        super(title);
        metadata = new MovieMetadata(director, year);
    }

    @Override
    public MediaMetadata getMetadata(){
        return metadata;
    }


}
