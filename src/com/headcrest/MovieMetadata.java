package com.headcrest;

public class MovieMetadata extends MediaMetadata {

    private final String director;
    private final String year;

    public MovieMetadata(String director, String year) {
        this.director = director;
        this.year = year;
    }

    @Override
    public String toString(){
        return String.format("Metadata - Director: %s, year: %s", director, year);
    }

}
