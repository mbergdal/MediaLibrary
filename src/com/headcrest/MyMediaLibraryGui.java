package com.headcrest;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.stage.Stage;



public class MyMediaLibraryGui extends Application {
    private ObservableList<MediaItem> allItems;
    private BorderPane borderPane;
    private ComboBox<String> mediaTypeCombo, addNewTypeCombo;
    private Button sortButton, addNewButton;
    private FilteredList<MediaItem> filteredData;
    private TextField newTitle = new TextField();

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {

        setUpData();
        setUpGUI();
        setUpHandlers();

        Scene scene = new Scene(borderPane);
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    private void setUpHandlers() {

        sortButton.setOnAction(e -> {
            allItems.sort((o1, o2) -> o1.getTitle().compareTo(o2.getTitle()));
        });

        mediaTypeCombo.setOnAction(e -> {
            String value = mediaTypeCombo.getSelectionModel().getSelectedItem();

            if (value.equals("All")){
                filteredData.setPredicate(x -> true);
            }
            if (value.equals("CD")){
                filteredData.setPredicate(x -> x instanceof CompactDisk);
            }
            if (value.equals("Movie")){
                filteredData.setPredicate(x -> x instanceof Movie);
            }
            if (value.equals("Vinyl")){
                filteredData.setPredicate(x -> x instanceof Vinyl);
            }
        });


        addNewButton.setOnAction(x -> {
            String title = newTitle.getText();
            String value = addNewTypeCombo.getSelectionModel().getSelectedItem();

            if (value.equals("CD")){
                allItems.add(new CompactDisk(title));
            }
            if (value.equals("Movie")){
                allItems.add(new Movie(title));
            }
            if (value.equals("Vinyl")){
                allItems.add(new Vinyl(title));
            }

            newTitle.clear();
        });
    }

    private void setUpGUI() {
        borderPane = new BorderPane();
        sortButton = new Button("Sort");
        mediaTypeCombo = new ComboBox<>();
        mediaTypeCombo.getItems().addAll("All", "Movie", "CD", "Vinyl");
        mediaTypeCombo.setValue("All");
        FlowPane buttons = new FlowPane(sortButton, mediaTypeCombo);
        filteredData = new FilteredList<>(allItems);
        borderPane.setCenter(new ListView<>(filteredData));
        borderPane.setBottom(buttons);

        newTitle.setPromptText("New Title");
        newTitle.setMinWidth(250);
        addNewTypeCombo = new ComboBox<>();
        addNewTypeCombo.getItems().addAll("Movie", "CD", "Vinyl");
        addNewTypeCombo.setValue("CD");
        addNewButton = new Button("Add");

        FlowPane addNewPane = new FlowPane(newTitle, addNewTypeCombo, addNewButton);
        addNewPane.alignmentProperty().set(Pos.CENTER_RIGHT);
        borderPane.setTop(addNewPane);
    }

    private void setUpData() {
        allItems = FXCollections.observableArrayList();
        allItems.add(new Movie("The Matrix", "Wachowski", "1999"));
        allItems.add(new CompactDisk("The Wall"));
        allItems.add(new Vinyl("Abbey Road"));
    }
}
