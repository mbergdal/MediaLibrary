package com.headcrest;

public class MediaItem {
    private String title;

    public MediaItem(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public MediaMetadata getMetadata() {
        return null;
    }

    @Override
    public String toString(){
        return title + " " + getMetadata();
    }
}
